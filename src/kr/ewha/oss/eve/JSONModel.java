package kr.ewha.oss.eve;

import java.util.List;

public class JSONModel {
	
    public static final String TAG_TOTAL_DISTANCE = "totalDistance";
    public static final String TAG_TOTAL_TIME = "totalTime";
    public static final String TAG_PLACEMARK = "Placemark";

    private double totalDistance;
    private int totalTime;
    private List<PlacemarkJson> placemarks;

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

	public List<PlacemarkJson> getPlacemarks() {
		return placemarks;
	}

	public void setPlacemarks(List<PlacemarkJson> placemarks) {
		this.placemarks = placemarks;
	}

    
}

class PlacemarkJson {
	private Coordinate coordinate;
	private int distance;
	private int time;
	private int turnType;
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getTurnType() {
		return turnType;
	}
	public void setTurnType(int turnType) {
		this.turnType = turnType;
	}
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
}

class Coordinate {
	private double latitude;
	private double longitude;
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
