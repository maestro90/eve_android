package kr.ewha.oss.eve;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class JSONBuilder {
	
	public JSONModel buildJSON(KML kml) {

		JSONModel model = new JSONModel();
		List<PlacemarkJson> placemarks = new ArrayList<PlacemarkJson>();
		PlacemarkJson placemark = null;
		
		String firstItem = null;
		
		for ( Placemark item : kml.getPlacemarks() ) {
			
			if ( item.getNodeType().equals("POINT")) {
				
				if ( firstItem == null )
					firstItem = "Point";

				if ( firstItem.equals("Point"))
					placemark = new PlacemarkJson();
				
				placemark.setTurnType(item.getTurnType());
				
				Coordinate coordinate = new Coordinate();
				String point =  ((Point)item.getPath()).getCoordinates();
				coordinate.setLatitude(Double.parseDouble(point.split(",")[0]));
				coordinate.setLongitude(Double.parseDouble(point.split(",")[1]));
				placemark.setCoordinate(coordinate);
				
				if ( !item.getName().equals("    ") ) {
					Log.i("Point", item.getName() + ", " + item.getName().length());
					placemark.setName(item.getName());
				} 
//				else
//					placemark.setName(" ");
				
				if ( firstItem.equals("Point"))
					placemarks.add(placemark);
				
			} 
			if ( item.getNodeType().equals("LINE")) {
				
				if ( firstItem == null )
					firstItem = "Line";
				
				if ( firstItem.equals("Line"))
					placemark = new PlacemarkJson();
				
				placemark.setDistance(item.getDistance());
				placemark.setTime(item.getTime());
				
				if ( !item.getName().equals("    ") ) {
					Log.i("Line", item.getName() + ", " + item.getName().length());
					placemark.setName(item.getName());
				}
//				else
//					placemark.setName(" ");
				
				if ( firstItem.equals("Line"))
					placemarks.add(placemark);
			}
		}
		
		model.setTotalDistance(kml.getTotalDistance());
		model.setTotalTime(kml.getTotalTime());
		model.setPlacemarks(placemarks);
		
		return model;
	}
	
	
}