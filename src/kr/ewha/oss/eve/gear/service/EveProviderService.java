package kr.ewha.oss.eve.gear.service;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;

import javax.security.cert.X509Certificate;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.accessory.SA;
import com.samsung.android.sdk.accessory.SAAgent;
import com.samsung.android.sdk.accessory.SAAuthenticationToken;
import com.samsung.android.sdk.accessory.SAPeerAgent;
import com.samsung.android.sdk.accessory.SASocket;

public class EveProviderService extends SAAgent {

	//static으로 선언해놓은 것 수정 요망 
	public static HashMap<Integer, GearProviderServiceConnection> mConnectionsMap = null;
	
	public static final String TAG = "GearService";

	private final IBinder mBinder = new LocalBinder();

	private boolean isAuthentication;

	public Context mContext = null;

	public static final int SERVICE_CONNECTION_RESULT_OK = 0;
	public static final int CONNECTION_ID = 1004;
	public static final int EVE_CHANNEL_ID = 104;

	public class LocalBinder extends Binder {
		public EveProviderService getService() {
			return EveProviderService.this;
		}
	}

	public EveProviderService() {
		super(TAG, GearProviderServiceConnection.class);
	}

	public class GearProviderServiceConnection extends SASocket {

		private int mConnectionId;

		public GearProviderServiceConnection() {
			super(GearProviderServiceConnection.class.getName());
		}

		@Override
		public void onError(int arg0, String arg1, int arg2) {

		}

		@Override
		public void onReceive(int channelId, byte[] data) {
			Log.d(TAG, "onReceive");
		}

		@Override
		protected void onServiceConnectionLost(int arg0) {

		}

	}

	@Override
	protected void onFindPeerAgentResponse(SAPeerAgent arg0, int arg1) {
		Log.d(TAG, "onFindPeerAgentResponse  arg1 =" + arg1);
	}

	@Override
	protected void onServiceConnectionRequested(SAPeerAgent peerAgent) {
		/*
		 * The authenticatePeerAgent(peerAgent) API may not be working properly
		 * depending on the firmware version of accessory device. Recommend to
		 * upgrade accessory device firmware if possible.
		 */

		// if(authCount%2 == 1)
		// isAuthentication = false;
		// else
		// isAuthentication = true;
		// authCount++;

		isAuthentication = false;

		if (isAuthentication) {
			Toast.makeText(getBaseContext(), "Authentication On!", Toast.LENGTH_SHORT).show();
			Log.e(TAG, "Start authenticatePeerAgent");
			authenticatePeerAgent(peerAgent);
		} else {
			Toast.makeText(getBaseContext(), "Authentication Off!", Toast.LENGTH_SHORT).show();
			Log.e(TAG, "acceptServiceConnectionRequest");
			acceptServiceConnectionRequest(peerAgent);
		}
	}

	@Override
	protected void onServiceConnectionResponse(SASocket thisConnection, int result) {
		if (result == CONNECTION_SUCCESS) {

			if (thisConnection != null) {
				GearProviderServiceConnection myConnection = (GearProviderServiceConnection) thisConnection;

				if (mConnectionsMap == null) {
					mConnectionsMap = new HashMap<Integer, GearProviderServiceConnection>();
				}

				myConnection.mConnectionId = (int) (System.currentTimeMillis() & 255);

				Log.d(TAG, "onServiceConnection connectionID = " + myConnection.mConnectionId);

				mConnectionsMap.put(CONNECTION_ID, myConnection);

			} else {
				Log.e(TAG, "SASocket object is null");
			}
		} else if (result == CONNECTION_ALREADY_EXIST) {
			Log.e(TAG, "onServiceConnectionResponse, CONNECTION_ALREADY_EXIST");
		} else {
			Log.e(TAG, "onServiceConnectionResponse result error =" + result);
		}
	}

	protected void onAuthenticationResponse(SAPeerAgent uPeerAgent, SAAuthenticationToken authToken, int error) {

		if (authToken.getAuthenticationType() == SAAuthenticationToken.AUTHENTICATION_TYPE_CERTIFICATE_X509) {
			mContext = getApplicationContext();
			byte[] myAppKey = getApplicationCertificate(mContext);

			if (authToken.getKey() != null) {
				boolean matched = true;
				if (authToken.getKey().length != myAppKey.length) {
					matched = false;
				} else {
					for (int i = 0; i < authToken.getKey().length; i++) {
						if (authToken.getKey()[i] != myAppKey[i]) {
							matched = false;
						}
					}
				}
				if (matched) {
					acceptServiceConnectionRequest(uPeerAgent);
					Log.e(TAG, "Auth-certification matched");
				} else
					Log.e(TAG, "Auth-certification not matched");

			}
		} else if (authToken.getAuthenticationType() == SAAuthenticationToken.AUTHENTICATION_TYPE_NONE)
			Log.e(TAG, "onAuthenticationResponse : CERT_TYPE(NONE)");
	}

	private static byte[] getApplicationCertificate(Context context) {
		if (context == null) {
			Log.e(TAG, "getApplicationCertificate ERROR, context input null");
			return null;
		}
		Signature[] sigs;
		byte[] certificat = null;
		String packageName = context.getPackageName();
		if (context != null) {
			try {
				PackageInfo pkgInfo = null;
				pkgInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
				if (pkgInfo == null) {
					Log.e(TAG, "PackageInfo was null!");
					return null;
				}
				sigs = pkgInfo.signatures;
				if (sigs == null) {
					Log.e(TAG, "Signature obtained was null!");
				} else {
					CertificateFactory cf = CertificateFactory.getInstance("X.509");
					ByteArrayInputStream stream = new ByteArrayInputStream(sigs[0].toByteArray());
					X509Certificate cert;
					cert = X509Certificate.getInstance(stream);
					certificat = cert.getPublicKey().getEncoded();
				}
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (javax.security.cert.CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return certificat;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "onCreate of smart view Provider Service");

		SA mAccessory = new SA();
		try {
			mAccessory.initialize(this);
		} catch (SsdkUnsupportedException e) {
			// Error Handling
		} catch (Exception e1) {
			Log.e(TAG, "Cannot initialize Accessory package.");
			e1.printStackTrace();
			/*
			 * Your application can not use Accessory package of Samsung Mobile
			 * SDK. You application should work smoothly without using this SDK,
			 * or you may want to notify user and close your app gracefully
			 * (release resources, stop Service threads, close UI thread, etc.)
			 */
			stopSelf();
		}
	}

}
