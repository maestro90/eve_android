package kr.ewha.oss.eve;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.skp.Tmap.TMapPOIItem;

public class DestinationSelectDialog extends Dialog {

	private ArrayList<TMapPOIItem> pois;
	private POISelectCallback listener;
	
	Context context;
	public DestinationSelectDialog(Context context) {
		super(context);
		this.context = context;
	}

	
	public void setPois(ArrayList<TMapPOIItem> pois) {
		this.pois = pois;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.destination_select_dialog);
		
		ListView listView = (ListView) findViewById(R.id.destination_list);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				listener.selectPOI(pois.get(position));
				dismiss();
			}
			
		});
		POIAdapter adapter = new POIAdapter();
		listView.setAdapter(adapter);
		
	
	}

	public void setPOISelectListener ( POISelectCallback listener ) {
		this.listener = listener;
	}
	
	public interface POISelectCallback {
		public void selectPOI(TMapPOIItem destination);
	}
	
	private class POIAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return pois.size();
		}

		@Override
		public Object getItem(int position) {
			return pois.get(position);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			
			if ( v == null ) {
				v = LayoutInflater.from(context).inflate(R.layout.destination_item, parent, false);
			}
		
			TextView nameTv = (TextView) v.findViewById(R.id.desti_name_tv);
			TextView addrTv = (TextView) v.findViewById(R.id.desti_addr_tv);
			
			nameTv.setText(pois.get(position).getPOIName().toString());
			addrTv.setText(pois.get(position).getPOIAddress().replace("null", ""));
			return v;
		}

		
	}
}
