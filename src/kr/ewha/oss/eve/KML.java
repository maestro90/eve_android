package kr.ewha.oss.eve;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 만종 on 2014-09-20.
 */
public class KML {
  /*
    TMap의 응답 객체 kml
   */

    public static final String TAG_TOTAL_DISTANCE = "totalDistance";
    public static final String TAG_TOTAL_TIME = "totalTime";
    public static final String TAG_PLACEMARK = "Placemark";


    private double totalDistance;
    private int totalTime;
    private List<Placemark> placemarks;

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public List<Placemark> getPlacemarks() {
        return placemarks;
    }

    public void setPlacemarks(List<Placemark> placemarks) {
        this.placemarks = placemarks;
    }
}

class Placemark {

    public static final String TAG_INDEX = "index";
    public static final String TAG_POINT_INDEX = "pointIndex";
    public static final String TAG_NAME = "name";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_NODE_TYPE = "nodeType";
    public static final String TAG_TURN_TYPE = "turnType";
    public static final String TAG_POINT_TYPE = "pointType";
    public static final String TAG_POINT = "Point";
    public static final String TAG_LINE_STRING = "LineString";
    public static final String TAG_DISTANCE = "distance";
    public static final String TAG_TIME = "time";
    
    private int index;
    private int pointIndex;
    private String name;
    private String description;
    private String nodeType;
    private int turnType;
    private String pointType;
    private Path path;
    private int time;
    private int distance;
    
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPointIndex() {
        return pointIndex;
    }

    public void setPointIndex(int pointIndex) {
        this.pointIndex = pointIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public int getTurnType() {
        return turnType;
    }

    public void setTurnType(int turnType) {
        this.turnType = turnType;
    }

    
    public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
}

class Path {
    public static final String TAG_COORDINATES = "coordinates";
}

class Point extends Path {

    private String coordinates;

    public Point() {
    }

    public Point(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }
}

class LineString extends Path {

    private List<String> coordinates;

    public LineString() {}

    public LineString(String[] coordinates) {

        this.coordinates = new ArrayList<String>();
        for (String coordinate : coordinates) {
            this.coordinates.add(coordinate);
        }
    }

    public List<String> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<String> coordinates) {
        this.coordinates = coordinates;
    }
}