package kr.ewha.oss.eve;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import kr.ewha.oss.eve.DestinationSelectDialog.POISelectCallback;
import kr.ewha.oss.eve.gear.service.EveProviderService;
import kr.ewha.oss.eve.gear.service.EveProviderService.GearProviderServiceConnection;

import org.w3c.dom.Document;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import tmapopenmapapi.ui.LogManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skp.Tmap.TMapAddressInfo;
import com.skp.Tmap.TMapData;
import com.skp.Tmap.TMapData.FindAllPOIListenerCallback;
import com.skp.Tmap.TMapData.FindPathDataAllListenerCallback;
import com.skp.Tmap.TMapData.FindPathDataListenerCallback;
import com.skp.Tmap.TMapData.TMapPathType;
import com.skp.Tmap.TMapData.reverseGeocodingListenerCallback;
import com.skp.Tmap.TMapGpsManager;
import com.skp.Tmap.TMapGpsManager.onLocationChangedCallback;
import com.skp.Tmap.TMapMarkerItem;
import com.skp.Tmap.TMapPOIItem;
import com.skp.Tmap.TMapPoint;
import com.skp.Tmap.TMapPolyLine;
import com.skp.Tmap.TMapView;
import com.skp.Tmap.TMapView.TMapLogoPositon;

public class MainActivity extends Activity implements onLocationChangedCallback, View.OnClickListener {

	@Override
	public void onLocationChange(Location location) {

		double lati = location.getLatitude();
		double longi = location.getLongitude();
		LogManager.printLog("onLocationChange " + location.getLatitude() + " " + location.getLongitude());
		setLocationPoint(longi, lati);
		setCurrentPosition(lati, longi);
	}

	private TMapView mMapView = null;

	private Context mContext;
	private ArrayList<Bitmap> mOverlayList;

	public static String mApiKey = "7b27ce59-6ba3-3431-a9b3-bfb00ef1b3fd"; // 발급받은
																			// appKey
	public static String mBizAppID; // 발급받은 BizAppID (TMapTapi로 TMap앱 연동을 할 때
									// BizAppID 꼭 필요)

	private boolean m_bShowMapIcon = false;

	ArrayList<String> mArrayMarkerID;
	private static int mMarkerID;

	TMapGpsManager gps = null;

	private Button btnFind;
	private TextView currentPositionTv;
	private EditText destinationEt;
	private Button destinationSearchBtn;

	private TMapPoint startPoint;
	private TMapPoint endPoint;

	private TMapData mapData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			startActivity(new Intent(this, SplashActivity.class));
		}

		setContentView(R.layout.activity_main);

		mMapView = (TMapView) findViewById(R.id.tmapView);
		btnFind = (Button) findViewById(R.id.btnFind);

		currentPositionTv = (TextView) findViewById(R.id.current_position_tv);
		destinationEt = (EditText) findViewById(R.id.destiation_addr_et);
		destinationSearchBtn = (Button) findViewById(R.id.destiation_search_btn);

		btnFind.setOnClickListener(this);
		destinationSearchBtn.setOnClickListener(this);

		mapData = new TMapData();

		mArrayMarkerID = new ArrayList<String>();

		mContext = this;

		m_bShowMapIcon = false;

		configureMapView();
		initView();

		gps = new TMapGpsManager(MainActivity.this);
		gps.setMinTime(1000000);
		gps.setMinDistance(5);

		gps.setProvider(gps.NETWORK_PROVIDER);
		gps.OpenGps();
		mMapView.setTMapLogoPosition(TMapLogoPositon.POSITION_BOTTOMRIGHT);
	}

	private void setCurrentPosition(final double lati, final double longi) {
		mapData.reverseGeocoding(lati, longi, "A00", new reverseGeocodingListenerCallback() {

			@Override
			public void onReverseGeocoding(final TMapAddressInfo info) {

				new Thread(new Runnable() {

					@Override
					public void run() {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								removeMapPath();
								removeAllMarkers();
								currentPositionTv.setText(info.strFullAddress);
								startPoint = new TMapPoint(lati, longi);
								showMarkerPoint(startPoint, "출발지", "여기가 출발지입니다.");
							}
						});
					}
				}).start();
				;
			}
		});
	}

	/**
	 * setSKPMapApiKey()에 ApiKey를 입력 한다. setSKPMapBizappId()에 mBizAppID를 입력한다.
	 * -> setSKPMapBizappId는 TMapTapi(TMap앱 연동)를 사용할때 BizAppID 설정 해야 한다.
	 * TMapTapi 사용하지 않는다면 setSKPMapBizappId를 하지 않아도 된다.
	 */
	private void configureMapView() {
		mMapView.setSKPMapApiKey(mApiKey);
		mMapView.setSKPMapBizappId(mBizAppID);
		mMapView.setTrackingMode(true);
	}

	/**
	 * initView - 버튼에 대한 리스너를 등록한다.
	 */
	private void initView() {
		mMapView.setOnApiKeyListener(new TMapView.OnApiKeyListenerCallback() {
			@Override
			public void SKPMapApikeySucceed() {
				LogManager.printLog("MainActivity SKPMapApikeySucceed");
			}

			@Override
			public void SKPMapApikeyFailed(String errorMsg) {
				LogManager.printLog("MainActivity SKPMapApikeyFailed " + errorMsg);
			}
		});

		mMapView.setOnBizAppIdListener(new TMapView.OnBizAppIdListenerCallback() {
			@Override
			public void SKPMapBizAppIdSucceed() {
				LogManager.printLog("MainActivity SKPMapBizAppIdSucceed");
			}

			@Override
			public void SKPMapBizAppIdFailed(String errorMsg) {
				LogManager.printLog("MainActivity SKPMapBizAppIdFailed " + errorMsg);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.destiation_search_btn:
			showDestinationDialog();
			break;
		case R.id.btnFind:
			drawPedestrianPath(startPoint, endPoint);
			break;
		}
	}

	/**
	 * EditText에 입력한 목적지에 해당하는 주소를 최대 20개까지 보여주는 다이얼로그 호출
	 */
	private void showDestinationDialog() {
		mapData.findAllPOI(destinationEt.getText().toString(), 20, new FindAllPOIListenerCallback() {

			@Override
			public void onFindAllPOI(final ArrayList<TMapPOIItem> pois) {

				new Thread(new Runnable() {

					@Override
					public void run() {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								DestinationSelectDialog dialog = new DestinationSelectDialog(MainActivity.this);
								dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
								dialog.setPois(pois);
								dialog.setPOISelectListener(poiCallback);
								dialog.show();
							}
						});
					}
				}).start();

			}
		});
	}

	/**
	 * 다이얼로그에서 선택받은 주소를 가져오는 Callback Method
	 */
	POISelectCallback poiCallback = new POISelectCallback() {

		@Override
		public void selectPOI(TMapPOIItem destination) {
			endPoint = destination.getPOIPoint();
			btnFind.setClickable(true);
			showMarkerPoint(endPoint, "도착지", "여기가 도착지입니다.");
		}
	};

	/**
	 * setLocationPoint 현재위치로 표시될 좌표의 위도,경도를 설정한다.
	 */
	public void setLocationPoint(double longi, double lati) {

		LogManager.printLog("setLocationPoint " + longi + " " + lati);

		mMapView.setLocationPoint(longi, lati);
	}

	/**
	 * setMapIcon 현재위치로 표시될 아이콘을 설정한다.
	 */
	public void setMapIcon() {
		m_bShowMapIcon = !m_bShowMapIcon;

		if (m_bShowMapIcon) {
			Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
			mMapView.setIcon(bitmap);
		}
		mMapView.setIconVisibility(m_bShowMapIcon);
	}

	public void showMarkerPoint(TMapPoint point, String name, String subTitle) {
		Bitmap bitmap = null;
		TMapMarkerItem item1 = new TMapMarkerItem();
		bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.i_location);

		item1.setTMapPoint(point);
		item1.setName(name);
		item1.setVisible(item1.VISIBLE);

		item1.setIcon(bitmap);
		LogManager.printLog("bitmap " + bitmap.getWidth() + " " + bitmap.getHeight());

		bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.i_location);
		item1.setCalloutTitle(name);
		item1.setCalloutSubTitle(subTitle);
		item1.setCanShowCallout(true);
		item1.setAutoCalloutVisible(true);

		Bitmap bitmap_i = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.i_go);

		// item1.setCalloutLeftImage(bitmap);
		item1.setCalloutRightButtonImage(bitmap_i);

		String strID = String.format("pmarker%d", mMarkerID++);

		mMapView.addMarkerItem(strID, item1);
		mArrayMarkerID.add(strID);

	}

	public void removeAllMarkers() {
		if (mArrayMarkerID.size() <= 0)
			return;

		for (String strMarkerID : mArrayMarkerID) {
			mMapView.removeMarkerItem(strMarkerID);
		}

		mArrayMarkerID.clear();

		Log.d("", Integer.toString(mArrayMarkerID.size()));
	}

	/**
	 * removeMapPath 경로 표시를 삭제한다.
	 */
	public void removeMapPath() {
		mMapView.removeTMapPath();

	}

	/**
	 * 경로를 탐색하고 결과를 xml 파싱한다.
	 * 
	 * @param startPoint
	 * @param endPoint
	 */
	public void drawPedestrianPath(TMapPoint startPoint, TMapPoint endPoint) {
		TMapPoint point1 = startPoint;
		TMapPoint point2 = endPoint;

		TMapData tmapdata = new TMapData();

		tmapdata.findPathDataWithType(TMapPathType.PEDESTRIAN_PATH, point1, point2, new FindPathDataListenerCallback() {

			@Override
			public void onFindPathData(TMapPolyLine polyLine) {

				polyLine.setLineColor(Color.BLUE);
				mMapView.addTMapPath(polyLine);

			}
		});

		tmapdata.findPathDataAllType(TMapPathType.PEDESTRIAN_PATH, point1, point2, new FindPathDataAllListenerCallback() {

			@Override
			public void onFindPathDataAll(Document document) {

				KML kml = null;
				String xmlString = xmlToString(document);
				writeToFile(xmlString);
				
				try {
					XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
					factory.setNamespaceAware(true);
					XmlPullParser xpp = factory.newPullParser();

					xpp.setInput(new StringReader(xmlString));

					String tagContent = null;
					Placemark placemark = null;

					int eventType = xpp.getEventType();
					while (eventType != XmlPullParser.END_DOCUMENT) {
						if (eventType == XmlPullParser.START_DOCUMENT) {
							kml = new KML();

						} else if (eventType == XmlPullParser.TEXT) {
							tagContent = xpp.getText();

						} else if (eventType == XmlPullParser.START_TAG) {
							if (xpp.getName().equals(KML.TAG_PLACEMARK)) {
								placemark = new Placemark();
							}

						} else if (eventType == XmlPullParser.END_TAG) {

							if (xpp.getName().equals(KML.TAG_TOTAL_DISTANCE)) {
								kml.setTotalDistance(Double.parseDouble(tagContent));
							}

							if (xpp.getName().equals(KML.TAG_TOTAL_TIME)) {
								kml.setTotalTime(Integer.parseInt(tagContent));
							}

							if (xpp.getName().equals(KML.TAG_PLACEMARK)) {
								if (kml.getPlacemarks() == null) {
									kml.setPlacemarks(new ArrayList<Placemark>());
								}
								kml.getPlacemarks().add(placemark);
							}

							if (xpp.getName().equals(Placemark.TAG_INDEX)) {
								placemark.setIndex(Integer.parseInt(tagContent));
							}

							if (xpp.getName().equals(Placemark.TAG_POINT_INDEX)) {
								placemark.setPointIndex(Integer.parseInt(tagContent));
							}

							if (xpp.getName().equals(Placemark.TAG_NAME)) {
								placemark.setName(tagContent);
							}

							if (xpp.getName().equals(Placemark.TAG_DESCRIPTION)) {
								placemark.setDescription(tagContent);
							}
							
							if ( xpp.getName().equals(Placemark.TAG_DISTANCE)) {
								placemark.setDistance(Integer.parseInt(tagContent));
							}
							
							if ( xpp.getName().equals(Placemark.TAG_TIME)) {
								placemark.setTime(Integer.parseInt(tagContent));
							}
							
							if (xpp.getName().equals(Placemark.TAG_NODE_TYPE)) {
								placemark.setNodeType(tagContent);
							}

							if (xpp.getName().equals(Placemark.TAG_TURN_TYPE)) {
								placemark.setTurnType(Integer.parseInt(tagContent));
							}

							if (xpp.getName().equals(Placemark.TAG_POINT_TYPE)) {
								placemark.setPointType(tagContent);
							}

							if (xpp.getName().equals(Path.TAG_COORDINATES)) {
								if (placemark.getNodeType().equals("POINT")) {
									placemark.setPath(new Point(tagContent));
								} else if (placemark.getNodeType().equals("LINE")) {
									String[] points = tagContent.split(" ");
									placemark.setPath(new LineString(points));
								}
							}
						}

						eventType = xpp.next();

					}
				} catch (Exception e) {
					e.printStackTrace();
				}


				Gson gson = new Gson();
				
				String result = gson.toJson(new JSONBuilder().buildJSON(kml), JSONModel.class);

				Log.i("MainActivity", result);
				sendToGear(result);
			}
			
            public void writeToFile(String s) {
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter("/sdcard/kml.xml"));
                    writer.write(s);
                    writer.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            

			public String xmlToString(Document doc) {
				try {
					StringWriter sw = new StringWriter();
					TransformerFactory tf = TransformerFactory.newInstance();
					Transformer transformer = tf.newTransformer();
					transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					transformer.setOutputProperty(OutputKeys.METHOD, "xml");
					transformer.setOutputProperty(OutputKeys.INDENT, "yes");
					transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

					transformer.transform(new DOMSource(doc), new StreamResult(sw));
					return sw.toString();
				} catch (Exception ex) {
					throw new RuntimeException("Error converting to String", ex);
				}
			}

			/**
			 * Gear로 json String을 보낸다. GearProviderServiceConnection은
			 * ProviderService에서 Callback 함수등으로 받아와야하나 현재로는 임시로 static으로 설정했다.
			 * 
			 * @param result
			 */
			private void sendToGear(String result) {
				GearProviderServiceConnection conn = EveProviderService.mConnectionsMap.get(EveProviderService.CONNECTION_ID);
				try {
					conn.send(EveProviderService.EVE_CHANNEL_ID, result.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		});

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		gps.CloseGps();
		if (mOverlayList != null) {
			mOverlayList.clear();
		}
		// Debug.stopMethodTracing();
		System.gc();
	}
}
